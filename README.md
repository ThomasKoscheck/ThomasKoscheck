# Hi, I'm Thomas

I'm a Webenthusiast, self employed at [blaugruen digital](https://blaugruen-digital.de)
Focus on web security

## 💼 Technical Skills

![](https://img.shields.io/badge/Code-Nuxt-informational?style=flat&logo=nuxtdotjs&color=00DC82)
![](https://img.shields.io/badge/Code-Vue-informational?style=flat&logo=vuedotjs&color=42b983)
![](https://img.shields.io/badge/Code-JavaScript-informational?style=flat&logo=JavaScript&color=F7DF1E)
![](https://img.shields.io/badge/Code-HTML5-informational?style=flat&logo=HTML5&color=E34F26)
![](https://img.shields.io/badge/Code-Python-informational?style=flat&logo=Python&color=003B57)

</br>

![](https://img.shields.io/badge/Style-Tailwind-informational?style=flat&logo=tailwindcss&color=22D3EE)
![](https://img.shields.io/badge/Style-CSS3-informational?style=flat&logo=CSS3&color=1572B6)

</br>

![](https://img.shields.io/badge/Tools-Docker-informational?style=flat&logo=Docker&color=2496ED)
![](https://img.shields.io/badge/Tools-GitLab-informational?style=flat&logo=GitLab&color=cb4e18)
![](https://img.shields.io/badge/Tools-AWS-informational?style=flat&logo=amazonaws&color=e76d0c)
![](https://img.shields.io/badge/Tools-Linux-informational?style=flat&logo=Linux&color=000000)
![](https://img.shields.io/badge/Tools-Postman-informational?style=flat&logo=Postman&color=FF6C37)
![](https://img.shields.io/badge/Tools-Netlify-informational?style=flat&logo=netlify&color=00C7B7)
![](https://img.shields.io/badge/Code-Firebase-informational?style=flat&logo=Firebase&color=003B57)
![](https://img.shields.io/badge/Tools-Git-informational?style=flat&logo=Git&color=F05032)


## Social Profiles

Follow me 👋 

- [@ThomasKoscheck on Twitter](https://twitter.com/ThomasKoscheck)
- [@ThomasKoscheck on LinkedIn](https://www.linkedin.com/in/ThomasKoscheck/)
- [Personal website thomaskoscheck.de](https://thomaskoscheck.de)
